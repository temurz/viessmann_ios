//
//  OnlineApplicationNavigator.swift
//  InfoMix
//
//  Created by Temur on 08/04/22.
//  Copyright © 2022 InfoMix. All rights reserved.
//

import Foundation
import UIKit

protocol OnlineApplicationNavigatorType {
    func showLogin(cardConfig: CardConfig)
}

struct OnlineApplicationNavigator : OnlineApplicationNavigatorType, ShowingLogin{
    unowned let assembler: Assembler
    unowned let navigationController: UINavigationController
    
    func showLogin(cardConfig: CardConfig) {
        self.showLogin(cardConfig: cardConfig, primaryAccount: true)
    }
}
