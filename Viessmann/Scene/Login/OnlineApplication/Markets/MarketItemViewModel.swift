//
//  MarketItemViewModel.swift
//  Viessmann
//
//  Created by Temur on 11/06/22.
//  Copyright © 2022 Viessmann. All rights reserved.
//

import Foundation

struct MarketItemViewModel : Identifiable{
    var id : Int
    var name: String?
    
    init(market: Market){
        self.id  = market.id
        self.name = market.name
    }
}
