//
//  MarketGateway.swift
//  Viessmann
//
//  Created by Temur on 11/06/22.
//  Copyright © 2022 Viessmann. All rights reserved.
//

import Foundation

protocol MarketGatewayType{
    func getMarkets(cityId: Int) -> Observable<[Market]>
}

struct MarketGateway : MarketGatewayType{
    func getMarkets(cityId: Int) -> Observable<[Market]> {
        let input = API.GetMarketsList(cityId: cityId)
        return API.shared.getMarketsList(input)
            .map { (output) -> [Market]? in
                return output
            }
            .replaceNil(with: [])
            .eraseToAnyPublisher()
    }
}
