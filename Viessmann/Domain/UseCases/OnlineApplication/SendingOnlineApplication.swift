//
//  SendingOnlineApplication.swift
//  InfoMix
//
//  Created by Temur on 08/04/22.
//  Copyright © 2022 InfoMix. All rights reserved.
//

import Foundation

protocol SendingOnlineApplication{
    var onlineApplicationGateway : OnlineApplicationGatewayType { get }
}

extension SendingOnlineApplication{
    func sendOnlineApplication(dto: OnlineApplicationDto) -> Observable<Bool>{
        onlineApplicationGateway.sendOnlineApplication(dto: dto)
    }
}
